package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.domain.ReservationStatusResponse;
import com.hotelReservation.hotelReservation.entity.Reservation;
import com.hotelReservation.hotelReservation.entity.Room;
import com.hotelReservation.hotelReservation.repository.IReservationRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ReservationServicesTest {

    private IReservationService iReservationService;

    @Mock
    private IReservationRepository repository;


    @Before
    public void setUp() {
        initMocks(this);
        iReservationService = new ReservationServices(repository);
    }


    @Test
    public void shouldAddReservation() {

        ReservationStatusResponse expected = ReservationStatusResponse.builder()
                .isPossible(true)
                .reason("Possible")
                .build();


        LocalDate begin = LocalDate.of(2018,10,12);
        LocalDate end = LocalDate.of(2018,10,14);

        Reservation reservation = getDefaultReservation();

        List<Reservation> reservationList = getReservationDatesListFromDb();

        Mockito.when(repository.save(reservation)).thenReturn(reservation);

        Mockito.when(repository.findReservationsByRoom(12L)).thenReturn(reservationList);

        ReservationStatusResponse actual = iReservationService.addReservation(begin,end,1L,1L,"YES","NO","NO",2);


        Assert.assertEquals(expected, actual);
        Mockito.verify(repository, Mockito.times(1)).save(reservation);

    }

    @Test
    public void ShouldDeleteReservationById() {

        Long id = 12L;

        Mockito.doNothing().when(repository).deleteById(id);

        iReservationService.cancelReservation(id);

        Mockito.verify(repository, Mockito.times(1)).deleteById(id);
    }

    @Test
    public void shouldFindReservation() {
        Long id = 1L;

        Reservation expected = getDefaultReservation();

        when(iReservationService.findSpecificReservation(id)).thenReturn(getDefaultReservation());

        Reservation actual = iReservationService.findSpecificReservation(id);

        Assert.assertThat(expected, is(actual));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionOnFindingReservationWithDoesntExist() {
        Long id = 1L;

      iReservationService.findSpecificReservation(id);

    }

    @Test
    public void shouldReturnListOfReservations() {
        Long id = 1L;
        List<Reservation> reservationListFromDb = getReservationDatesListFromDb();

        when(iReservationService.findAllReservationsOnRoom(id)).thenReturn(reservationListFromDb);

        List<Reservation> actual = iReservationService.findAllReservationsOnRoom(id);

        Assert.assertArrayEquals(reservationListFromDb.toArray(), actual.toArray());
    }


    private List<Reservation> getReservationDatesListFromDb() {
        Reservation first = Reservation.builder()
                .begin(LocalDate.of(2018, 05, 01))
                .end(LocalDate.of(2018, 05, 03))
                .build();
        Reservation second = Reservation.builder()
                .begin(LocalDate.of(2017, 05, 01))
                .end(LocalDate.of(2017, 05, 03))
                .build();
        return Arrays.asList(first, second);
    }


    private Reservation getDefaultReservation() {
        return Reservation.builder()
                .begin(LocalDate.of(2018, 7, 23))
                .end(LocalDate.of(2018, 7, 29))
                .reservationId(1234L)
                .roomForReservation(getDefaultRoom())
                .rentCar("NO")
                .breakfast("NO")
                .dinner("NO")
                .adultsForReservation(2)
                .build();
    }

    public Room getDefaultRoom() {
        return Room.builder()
                .howManyPeople(2)
                .id(12L)
                .roomClass("Hobbit")
                .build();
    }
}