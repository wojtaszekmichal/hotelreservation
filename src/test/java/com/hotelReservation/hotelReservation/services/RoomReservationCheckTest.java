//package com.hotelReservation.hotelReservation.services;
//
//import com.hotelReservation.hotelReservation.domain.RoomReservationDates;
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.time.LocalDate;
//
//public class RoomReservationCheckTest {
//
//    @Test
//    public void canBookRoomWhenWantedBeginBeforeExistingBefore(){
//        RoomReservationDates roomReservationDates = new RoomReservationDates(LocalDate.of(2018,7,3),LocalDate.of(2018,7,5));
//
//        RoomReservationDates wanted = new RoomReservationDates(LocalDate.of(2018,7,1),LocalDate.of(2017,7,2));
//
//        boolean actual = RoomReservationCheck.canBookRoom(roomReservationDates, wanted);
//
//        Assert.assertTrue(actual);
//    }
//
//    @Test
//    public void canBookRoomWhenWantedBeginAfterExistingEnd(){
//        RoomReservationDates roomReservationDates = new RoomReservationDates(LocalDate.of(2018,7,3),LocalDate.of(2018,7,5));
//
//        RoomReservationDates wanted = new RoomReservationDates(LocalDate.of(2018,7,6),LocalDate.of(2017,7,7));
//
//        boolean actual = RoomReservationCheck.canBookRoom(roomReservationDates, wanted);
//
//        Assert.assertTrue(actual);
//    }
//
//    @Test
//    public void cantBookDueToExistingBeginReservation(){
//        RoomReservationDates roomReservationDates = new RoomReservationDates(LocalDate.of(2018,7,3),LocalDate.of(2018,7,5));
//
//        RoomReservationDates wanted = new RoomReservationDates(LocalDate.of(2018,7,4),LocalDate.of(2017,7,7));
//
//        boolean actual = RoomReservationCheck.canBookRoom(roomReservationDates, wanted);
//
//        Assert.assertFalse(actual);
//    }
//
//    @Test
//    public void cantBookWhenWantedEndBetweenDates(){
//        RoomReservationDates roomReservationDates = new RoomReservationDates(LocalDate.of(2018,7,3),LocalDate.of(2018,7,5));
//
//        RoomReservationDates wanted = new RoomReservationDates(LocalDate.of(2018,7,2),LocalDate.of(2018,7,4));
//
//        boolean actual = RoomReservationCheck.canBookRoom(roomReservationDates, wanted);
//
//        Assert.assertFalse(actual);
//    }
//
//
//
//    @Test
//    public void canBookWhenWantedEndOutOfExistingDates(){
//        RoomReservationDates roomReservationDates = new RoomReservationDates(LocalDate.of(2018,7,3),LocalDate.of(2018,7,5));
//
//        RoomReservationDates wanted = new RoomReservationDates(LocalDate.of(2018,7,6),LocalDate.of(2017,7,8));
//
//        boolean actual = RoomReservationCheck.canBookRoom(roomReservationDates, wanted);
//
//        Assert.assertTrue(actual);
//    }
//}