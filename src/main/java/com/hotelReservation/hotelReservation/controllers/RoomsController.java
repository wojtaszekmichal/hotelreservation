package com.hotelReservation.hotelReservation.controllers;

import com.hotelReservation.hotelReservation.entity.Room;
import com.hotelReservation.hotelReservation.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class RoomsController {

    private RoomService roomService;

    @Autowired
    public RoomsController(RoomService roomService) {
        this.roomService = roomService;
    }

    @GetMapping(value = "/rooms")
    public String allBooksPage(Model model) {
        List<Room> allRooms = roomService.findAllRooms();
        model.addAttribute("rooms", allRooms);
        return "rooms";
    }

    @GetMapping(value = "/rooms/{id}/{error}")
    public String showRoomDetails(@PathVariable("id") Long id, Model model, @PathVariable(required = false, name = "error") String error){
        Room roomById = roomService.findRoomById(id);
        model.addAttribute("room",roomById);
        if(error.equals("DateTaken")) model.addAttribute("error","DateTaken");

        return "roomDetails";
    }
}
