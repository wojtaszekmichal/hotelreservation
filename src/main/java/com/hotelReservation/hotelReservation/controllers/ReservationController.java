package com.hotelReservation.hotelReservation.controllers;

import com.hotelReservation.hotelReservation.domain.ReservationStatusResponse;
import com.hotelReservation.hotelReservation.entity.Reservation;
import com.hotelReservation.hotelReservation.entity.Room;
import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
public class ReservationController {
    private ReservationServices reservationServices;
    private RoomService roomService;
    private UserService userService;

    @Autowired
    public ReservationController(ReservationServices reservationServices, RoomService roomService, UserService userService) {
        this.reservationServices = reservationServices;
        this.roomService = roomService;
        this.userService = userService;
    }

    @GetMapping(value = "/myReservations")
    public String showReservations(Model model) {
        List<Reservation> all = reservationServices.findAll();
        model.addAttribute("reservations", all);
        return "MyReservations";
    }



    @GetMapping(value = "reservation/pick/{id}")
    @ResponseBody
    public ResponseEntity<Reservation> ShowOneReservation(@PathVariable Long id) {
        Reservation specificReservation = reservationServices.findSpecificReservation(id);
        if (specificReservation != null) {
            return new ResponseEntity<>(specificReservation, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/reservation/{id}")
    public String makeReservation(@PathVariable Long id, Model model, @RequestParam String beginDate, @RequestParam int adultsForReservation,
                                  @RequestParam String endDate, @RequestParam(required = false) String breakfast,
                                  @RequestParam(required = false) String dinner, @RequestParam(required = false) String carRent) {

        LocalDate begin = LocalDate.parse(beginDate);
        LocalDate end = LocalDate.parse(endDate);
        Room roomById = roomService.findRoomById(id);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userIdByName = userService.getUserIdByName(authentication.getName());
        if (breakfast == null) breakfast = "NO";
        if (dinner == null) dinner = "NO";
        if (carRent == null) carRent = "NO";
        model.addAttribute("room", roomById);
        ReservationStatusResponse reservationStatusResponse = reservationServices.addReservation(begin, end, id, userIdByName.getUserId(),
                breakfast, dinner, carRent,adultsForReservation);
        switch (reservationStatusResponse.getReason()) {
            case "Possible":
                List<Reservation> all = reservationServices.findAll();

                model.addAttribute("reservations", all);
                return "MyReservations";
            default:
                return "redirect:/rooms/" + id + "/DateTaken";
        }
    }
}
