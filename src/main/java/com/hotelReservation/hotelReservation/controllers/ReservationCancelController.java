package com.hotelReservation.hotelReservation.controllers;

import com.hotelReservation.hotelReservation.entity.Reservation;
import com.hotelReservation.hotelReservation.services.ReservationServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ReservationCancelController {

    private ReservationServices reservationServices;

    @Autowired
    public ReservationCancelController(ReservationServices reservationServices) {
        this.reservationServices = reservationServices;
    }

    @PostMapping(value = "/reservationCancel/{id}")
    public String cancelReservation(@PathVariable Long id, Model model){
        reservationServices.cancelReservation(id);
        List<Reservation> all = reservationServices.findAll();
        model.addAttribute("reservations", all);
        return "MyReservations";
    }
}
