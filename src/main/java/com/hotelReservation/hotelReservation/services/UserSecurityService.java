package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.domain.UserPrincipalDetails;
import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserSecurityService implements UserDetailsService {

    @Autowired
    private IUserRepository iUserRepository;

    public UserDetails loadUserByUsername(String username) {
    User u = iUserRepository.findUserByUsername(username);
    if (u == null) {
        throw new UsernameNotFoundException(username);
    }
    return new UserPrincipalDetails(u);
}

}
