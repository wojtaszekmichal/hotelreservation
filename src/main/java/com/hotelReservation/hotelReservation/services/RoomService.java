package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.entity.Room;
import com.hotelReservation.hotelReservation.repository.IRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService implements IRoomService {
    private IRoomRepository iRoomRepository;

    @Autowired
    public RoomService(IRoomRepository iRoomRepository) {
        this.iRoomRepository = iRoomRepository;
    }

    @Override
    public void addRoom(Room room) {
        iRoomRepository.save(room);
    }

    @Override
    public void editRoom(Long id, Room room) {
        iRoomRepository.deleteById(id);
        iRoomRepository.save(room);
    }

    @Override
    public Room findRoomById(Long id) {
        return iRoomRepository.findById(id).orElse(null);
    }

    @Override
    public List<Room> findAllRoomsByHowManyPeople(int howManyPeople) {
        return iRoomRepository.findAllByHowManyPeople(howManyPeople);
    }

    @Override
    public List<Room> findAllRoomsByClassRoom(String roomClass) {
        return iRoomRepository.findAllByRoomClass(roomClass);
    }

    @Override
    public List<Room> findAllRooms() {
        return iRoomRepository.findAll();
    }

    @Override
    public List<Room> FindRoomByIsRent(boolean isRent) {
        return iRoomRepository.findAllByTheRent(isRent);
    }
}
