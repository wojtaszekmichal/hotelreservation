package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.domain.ReservationStatusResponse;
import com.hotelReservation.hotelReservation.entity.Reservation;
import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.repository.IReservationRepository;
import com.hotelReservation.hotelReservation.repository.IRoomRepository;
import com.hotelReservation.hotelReservation.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationServices implements IReservationService {

    private IReservationRepository iReservationRepository;
    private IRoomRepository iRoomRepository;
    private IUserRepository iUserRepository;

    @Autowired
    public ReservationServices(IReservationRepository iReservationRepository) {
        this.iReservationRepository = iReservationRepository;
        this.iRoomRepository = iRoomRepository;
        this.iUserRepository = iUserRepository;
    }

    @Override
    public ReservationStatusResponse addReservation(LocalDate begin, LocalDate end, Long roomId, Long userId, String breakfast, String diner, String rentCar, int adultsForReservation) {
        PriceCounter priceCounter = new PriceCounter();
        int days = 0;
        if(end.isAfter(begin)){
        days = begin.until(end).getDays();
        }
        int price = priceCounter.calculatePrice(breakfast, diner, rentCar, days, adultsForReservation);
        Reservation newReservation = Reservation.builder()
                .begin(begin)
                .end(end)
                .roomForReservation(iRoomRepository.findById(roomId).get())
                .user(iUserRepository.findById(userId).get())
                .breakfast(breakfast)
                .dinner(diner)
                .rentCar(rentCar)
                .adultsForReservation(adultsForReservation)
                .price(price)
                .build();

        ReservationStatusResponse reservationStatusResponse = checkIsReservationDatesAreNotTaken(newReservation.getRoomForReservation()
                .getId(), newReservation.getBegin(), newReservation.getEnd());
        if (reservationStatusResponse.isPossible()) {
            iReservationRepository.save(newReservation);
        }
        return reservationStatusResponse;
    }

    @Override
    public void cancelReservation(Long id) {
        iReservationRepository.deleteById(id);
    }

    public List<Reservation> findAllReservationOfSpecificUser(Long id) {
        List<Reservation> allByUserId = iReservationRepository.findAllByUserId(id);
        return new ArrayList<>(allByUserId);
    }

    public Reservation findSpecificReservation(Long id) throws NullPointerException{
        Reservation reservation = null;
        Optional<Reservation> byId = iReservationRepository.findById(id);
        if(byId.isPresent()){
            reservation = byId.get();
        }
        return reservation;
    }

    public List<Reservation> findAllReservationsOnRoom(Long roomId) {
        return iReservationRepository.findReservationsByRoom(roomId);
    }

    @Override
    public ReservationStatusResponse checkIsReservationDatesAreNotTaken(Long roomId, LocalDate begin, LocalDate end) {
        ReservationStatusResponse reservationStatusResponse = new ReservationStatusResponse();
        if(begin.isAfter(end) || begin.isBefore(LocalDate.now())){
                        reservationStatusResponse.setPossible(false);
            reservationStatusResponse.setReason("Begin date is after end date or before today !");
            return reservationStatusResponse;
        }
        int newReservationPeriod = begin.until(end).getDays();
        List<LocalDate> newReservationDates = new ArrayList<>();
        List<LocalDate> takenReservationDates = new ArrayList<>();
        for(int i = 0;i<=newReservationPeriod;i++){
            newReservationDates.add(begin.plusDays(i));
        }
        List<Reservation> allReservationsOnRoom = findAllReservationsOnRoom(roomId);
        for (Reservation r :
                allReservationsOnRoom) {
            int existingReservationPeriod = r.getBegin().until(r.getEnd()).getDays();
            for(int i = 0;i<=existingReservationPeriod;i++){
                takenReservationDates.add(r.getBegin().plusDays(i));
            }
            for (LocalDate l :
                    takenReservationDates) {
                for (LocalDate lo :
                        newReservationDates) {
                  if ( l.isEqual(lo)){
                      reservationStatusResponse.setReason("Date is taken");
                      reservationStatusResponse.setPossible(false);
                      return reservationStatusResponse;
                  }
                }
            }
        }
        reservationStatusResponse.setPossible(true);
        reservationStatusResponse.setReason("Possible");
        return reservationStatusResponse;
    }


    //    public ReservationStatusResponse checkIsReservationDatesAreNotTaken(Long roomId, LocalDate begin, LocalDate end) {
//        ReservationStatusResponse status = new ReservationStatusResponse();
//        Period until = begin.until(end);
//        int days = until.getDays();
//        if (begin.isAfter(end) || begin.isBefore(LocalDate.now()) || days > 7) {
//            status.setPossible(false);
//            status.setReason("Begin date is after end date or before today !");
//            return status;
//        } else {
//            RoomReservationDates wanted = new RoomReservationDates(begin, end);
//
//            List<Reservation> allReservationsOnRoom = findAllReservationsOnRoom(roomId);
//            for (Reservation r :
//                    allReservationsOnRoom) {
//
//                RoomReservationDates existing = new RoomReservationDates(r.getBegin(), r.getEnd());
//                if (!RoomReservationCheck.canBookRoom(existing,wanted)) {
//                    status.setPossible(false);
//                    status.setReason("There is existing reservation on this date.");
//                    status.setReservationsTakenOnDate(r);
//                    return status;
//                }
//            }
//
//            status.setPossible(true);
//            status.setReason("possible");
//        }
//        return status;
//    }

    @Override
    public List<Reservation> findAll() {
        return iReservationRepository.findAll();
    }



}
