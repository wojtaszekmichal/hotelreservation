package com.hotelReservation.hotelReservation.services;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PriceCounter {

    private int breakfastPrice = 5;
    private int dinnerPrice = 7;
    private int rentCarPrice = 12;
    private int dayCost = 10;


    public int calculatePrice(String breakfast, String dinner, String rentCar, int rentPeriod, int adults) {
        int totalPrice = 0;
        totalPrice += rentPeriod * dayCost * adults;
        if (breakfast.equals("YES")) {
            totalPrice += breakfastPrice * adults * rentPeriod;
        }
        if (dinner.equals("YES")) {
            totalPrice += dinnerPrice * adults * rentPeriod;
        }
        if (rentCar.equals("YES")) {
            totalPrice += rentCarPrice * rentPeriod;
        }
        return totalPrice;
    }
}
