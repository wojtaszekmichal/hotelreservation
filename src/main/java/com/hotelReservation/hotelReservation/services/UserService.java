package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.repository.IUserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserIdByName(String userName){
        return userRepository.findUserByUsername(userName);
    }
}
