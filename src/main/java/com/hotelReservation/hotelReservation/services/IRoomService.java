package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.entity.Room;

import java.util.List;

public interface IRoomService {

    void addRoom(Room room);

    void editRoom(Long id, Room room);

    Room findRoomById(Long id);

    List<Room> findAllRoomsByHowManyPeople(int howManyPeople);

    List<Room> findAllRoomsByClassRoom(String roomClass);

    List<Room> findAllRooms();

    List<Room> FindRoomByIsRent(boolean isRent);
}
