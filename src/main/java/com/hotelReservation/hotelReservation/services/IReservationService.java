package com.hotelReservation.hotelReservation.services;

import com.hotelReservation.hotelReservation.domain.ReservationStatusResponse;
import com.hotelReservation.hotelReservation.entity.Reservation;
import com.hotelReservation.hotelReservation.entity.User;

import java.time.LocalDate;
import java.util.List;

public interface IReservationService {

    ReservationStatusResponse addReservation(LocalDate begin, LocalDate end, Long roomId,
                                             Long userId,String breakfast, String diner, String carRent,int adultsForReservation);

    void cancelReservation(Long id);

    List<Reservation> findAllReservationOfSpecificUser(Long id);

    Reservation findSpecificReservation(Long id);

    List<Reservation> findAllReservationsOnRoom(Long roomId);

    ReservationStatusResponse checkIsReservationDatesAreNotTaken(Long roomId, LocalDate begin, LocalDate end);

    List<Reservation> findAll();
}
