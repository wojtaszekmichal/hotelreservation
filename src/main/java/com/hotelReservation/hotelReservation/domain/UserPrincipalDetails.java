package com.hotelReservation.hotelReservation.domain;

import com.hotelReservation.hotelReservation.entity.User;
import org.springframework.security.core.GrantedAuthority;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;

import java.util.Collection;

public class UserPrincipalDetails implements UserDetails {
    private User user;

    public UserPrincipalDetails(User user) {
        this.user = user;

    }

    @Override

    public Collection<? extends GrantedAuthority> getAuthorities() {

        return Arrays.asList(new SimpleGrantedAuthority("USER"), new SimpleGrantedAuthority("ADMIN"));

    }

    @Override

    public String getPassword() {

        return user.getPassword();

    }

    @Override

    public String getUsername() {

        return user.getUsername();

    }

    @Override

    public boolean isAccountNonExpired() {

        return true;

    }

    @Override

    public boolean isAccountNonLocked() {

        return true;

    }

    @Override

    public boolean isCredentialsNonExpired() {

        return true;

    }

    @Override

    public boolean isEnabled() {

        return true;

    }

//    @Override
//
//    public boolean equals(Object o) {
//
//        if (this == o) return true;
//        if (!(o instanceof UserPrincipalDetails)) return false;
//        UserPrincipalDetails that = (UserPrincipalDetails) o;
//        return new EqualsBuilder()
//                .append(user, that.user)
//                .isEquals();
//    }
//
//    @Override
//
//    public int hashCode() {
//        return new HashCodeBuilder(17, 37)
//                .append(user)
//                .toHashCode();
//    }

}
