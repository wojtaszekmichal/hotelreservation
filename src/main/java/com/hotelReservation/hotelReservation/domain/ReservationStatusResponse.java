package com.hotelReservation.hotelReservation.domain;

import com.hotelReservation.hotelReservation.entity.Reservation;
import lombok.*;

import java.util.List;
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationStatusResponse {
    private boolean isPossible;
    private Reservation reservationsTakenOnDate;
    private String reason;
}
