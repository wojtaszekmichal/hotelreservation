package com.hotelReservation.hotelReservation.domain;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ReservationDetails {

    String beginDate;
    String adultsForReservation;
    String endDate;
    String breakfast;
    String dinner;
    String carRent;

}
