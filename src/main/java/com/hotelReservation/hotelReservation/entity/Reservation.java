package com.hotelReservation.hotelReservation.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Reservation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long reservationId;
    @ManyToOne
    @JoinColumn(name = "listReservation")
    private User user;
//    @Convert(converter = LocalDateTimeConverter.class)
    @Column(nullable = false)
    private LocalDate begin;
    @Column(nullable = false)
    private LocalDate end;
    private boolean isConfirm = false;
    private int adultsForReservation;
    private String breakfast;
    private String dinner;
    private String rentCar;
    private int price;
    @ManyToOne
    @JoinColumn(name = "listOfReservations")
    private Room roomForReservation;
}
