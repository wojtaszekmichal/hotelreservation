package com.hotelReservation.hotelReservation.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "room")
public class Room implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long id;
    private boolean theRent;
    private boolean TheReservation;
    @Column(nullable = false)
    private int howManyPeople;
    @Column(nullable = false)
    private String roomClass;
    @Column(nullable = false)
    private int tvInRoom;
    @Column(nullable = false)
    private int bedsInRoom;
    @Column(nullable = false)
    private String photoOne;
    @Column(nullable = false)
    private String photoTwo;
    @Column(nullable = false)
    private String photoThree;
    @OneToMany(mappedBy = "rentId")
    private List<Rent> listOfRents;
    @JsonIgnore
    @OneToMany(mappedBy = "roomForReservation")
    private List<Reservation> listOfReservations;
}
