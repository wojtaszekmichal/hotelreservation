package com.hotelReservation.hotelReservation.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Rent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rentId;
    @ManyToOne
    @JoinColumn(name = "listOfRents")
    private Room roomId;
    @ManyToOne
    @JoinColumn(name = "rentsList")
    private User user;
    @Column(nullable = false)
    private boolean isPayed = false;
    @Column(nullable = false)
    private boolean isConfirmed = false;
}
