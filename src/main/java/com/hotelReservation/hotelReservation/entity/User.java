package com.hotelReservation.hotelReservation.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String role;
    @Column(nullable = false)
    private String email;
    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Rent> rentsList;
    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<Reservation> listReservation;
}
