package com.hotelReservation.hotelReservation;

import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.repository.IUserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class HotelReservationApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelReservationApplication.class, args);

    }

    @Bean
    CommandLineRunner runner(IUserRepository iuserRepository, PasswordEncoder passwordEncoder) {

        return args -> {
            User user = User.builder()
                    .username("user")
                    .role("USER")
					.email("user@gmail.pl")
                    .password(passwordEncoder.encode("user"))
                    .build();
            User secondUser = User.builder()
                    .username("JanKowalski")
                    .role("USER")
                    .email("janek@kowalski.pl")
                    .password(passwordEncoder.encode("janek123"))
                    .build();

            if (iuserRepository.findUserByUsername(user.getUsername()) == null) {
                iuserRepository.save(user);
                iuserRepository.save(secondUser);
            }
        };
    }
}
