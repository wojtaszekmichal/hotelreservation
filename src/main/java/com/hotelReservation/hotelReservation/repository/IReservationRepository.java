package com.hotelReservation.hotelReservation.repository;

import com.hotelReservation.hotelReservation.entity.User;
import com.hotelReservation.hotelReservation.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IReservationRepository extends JpaRepository<Reservation,Long> {

    List<Reservation> findAllByUser(User user);

    @Query("select re from Reservation  re inner join re.user ro where ro.userId = :id")
    List<Reservation> findAllByUserId(Long id);

    @Query("select r from Reservation r inner join r.roomForReservation ro where ro.id = :id")
    List<Reservation> findReservationsByRoom(@Param ("id") Long id);


}
