package com.hotelReservation.hotelReservation.repository;

import com.hotelReservation.hotelReservation.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IRoomRepository extends JpaRepository<Room,Long> {

     List<Room> findAllByHowManyPeople(int howManyPeople);

     List<Room> findAllByRoomClass(String roomClass);

     List<Room> findAllByTheRent(boolean theRent);

}
