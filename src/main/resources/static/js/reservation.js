var onButtonClick = function (id) {
    $.ajax({
        type: "GET",
        url: "/reservation/pick/" + id,
        success: function (result) {
            $('#breakfastInput').empty().append(result.breakfast);
            $('#dinnerInput').empty().append(result.dinner);
            $('#rentCarInput').empty().append(result.rentCar);
            $('#howManyPeopleInput').empty().append(result.adultsForReservation);
            $('#roomClassInput').empty().append(result.roomForReservation.roomClass);
            $('#beginInput').empty().append(result.begin);
            $('#endInput').empty().append(result.end);
            $('#priceInput').empty().append(result.price);
            $('#cancelForm').empty().append("<form action=\"/reservationCancel/" + result.reservationId + "\" method=\"post\">" +
                "<button type=\"submit\" class=\"btn btn-primary\">Cancel reservation</button></form>");
        },
        error: function (result) {
            console.log(result);
        }
    });
};
